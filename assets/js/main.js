//jQuery is required to run this code
$( document ).ready(function() {

    scaleVideoContainer();

    initBannerVideoSize('.video-container .poster img');
    initBannerVideoSize('.video-container .filter');
    initBannerVideoSize('.video-container video');

    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video-container .poster img');
        scaleBannerVideoSize('.video-container .filter');
        scaleBannerVideoSize('.video-container video');
    });

});

function scaleVideoContainer() {

    var height = $(window).height() + 5;
    var unitHeight = parseInt(height) + 'px';
    $('.homepage-hero-module').css('height',unitHeight);

}

function initBannerVideoSize(element){

    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
        windowHeight = $(window).height() + 5,
        videoWidth,
        videoHeight;

    // console.log(windowHeight);

    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width');

        $(this).width(windowWidth);

        if(windowWidth < 1000){
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});

            $(this).width(videoWidth).height(videoHeight);
        }

        $('.homepage-hero-module .video-container video').addClass('fadeIn animated');

    });
}

$('#menuModal').on('show.bs.modal', function () {
    //alert('hey');
    $('header.fixed-top').css({'z-index': '1060',
        'transform': 'translate(0%, 0%) matrix(1, 0, 0, 1, 0, 0)',
        //'background':'#000000'
    });
});

$('.modal').on('hidden.bs.modal', function () {
    $('body').css('overflow', 'auto');
});
$(window).resize(function () {
    $('.modal').on('show.bs.modal', function () {
        $('.fullbody').css('height', $(window).height() * 0.88);
        $('.fullbody').css('overflow-y', 'auto');
        $('header.fixed-top').css({'z-index': '1060',
            'transform': 'translate(0%, 0%) matrix(1, 0, 0, 1, 0, 0)'
        });
    });
});
/***** Aos */
AOS.init({
    //duration: 400,
    easing: 'ease-in-out',
    //delay: 200,
    disable: 'mobile',
    //once: 'true',
    offset: 50
});

/* match height */
$('[data-plugin="matchHeight"]').each(function() {
    var $this = $(this),
        options = $.extend(true, {}, null, $this.data()),
        matchSelector = $this.data('matchSelector');

    if (matchSelector) {
        $this.find(matchSelector).matchHeight(options);
    } else {
        $this.children().matchHeight(options);
    }

});
if ($(window).width() < 576) {
    $('[data-plugin="matchHeight"]').matchHeight({ remove: true });
}

$('[data-toggle="tooltip"]').tooltip();

var iScrollPos = 0;
$(window).scroll(function () {
    var iCurScrollPos = $(this).scrollTop();
    if (iCurScrollPos > iScrollPos) {
        //Scrolling Down
        $('header.fixed-top').css({'transform': 'translate(0%, -100%) matrix(1, 0, 0, 1, 0, 0)',
            'transition': 'all 0.5s',
            'background-color':'transparent'});
    } else {
        //Scrolling Up
        $('header.fixed-top').css({'transform': 'translate(0%, 0%) matrix(1, 0, 0, 1, 0, 0)',
            'transition': 'all 0.5s'});
    }
    iScrollPos = iCurScrollPos;
});



